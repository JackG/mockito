package com.j4soft;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.misusing.MissingMethodInvocationException;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSuite {

    @Mock
    private Calculator calculator;

    @InjectMocks
    private CalculatorService simpleService;

    @Test
    public void testAdd() {
        when(calculator.add(1, 2)).thenReturn(3);
        int result = simpleService.add(1, 2);
        assertEquals(3, result);
    }

    @Test
    public void testCallCalculatorAddOnce() {
        when(calculator.add(1, 2)).thenReturn(3);
        simpleService.add(1, 2);
        verify(calculator, times(1)).add(1, 2);
    }

    @Test
    public void testSpyAddCalculator() {
        Calculator spiedCalculator = spy(new Calculator());
        int result = spiedCalculator.add(3, 4);
        assertEquals(7, result);
        verify(spiedCalculator, times(1)).add(3, 4);
    }

    @Test (expected = MissingMethodInvocationException.class)
    public void testCallingStaticMethodSubtract() {
        when(Calculator.subtract(4, 2)).thenReturn(2);
/*
        int result = simpleService.subtract(4, 2);
        assertEquals(2, result);
*/
    }
}
