package com.j4soft;

public class CalculatorService {

    Calculator calculator = new Calculator();

    public int add(int value1, int value2) {
        return calculator.add(value1, value2);
    }

    public static int subtract(int value1, int value2) {
        return Calculator.subtract(value1, value2);
    }
}
